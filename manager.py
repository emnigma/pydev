from suff_tree import Node, insert, keys_with_prefix, str_hash

class Geoname:

    def __init__(self, data):

        data = data.split("\t")
        self.geonameid = int(data[0])
        self.name = data[1]
        self.asciiname = data[2]
        self.alternatenames = data[3].split(',')
        self.latitude = data[4]
        self.longitude = data[5]
        self.feature_class = data[6]
        self.feature_code = data[7]
        self.country_code = data[8]
        self.cc2_codes = data[9].split(',')
        self.admin1_code = data[10]
        self.admin2_code = data[11]
        self.admin3_code = data[12]
        self.admin4_code = data[13]
        self.population = int(data[14])
        self.elevation = data[15]
        self.dem = int(data[16])
        self.timezone = data[17]
        self.modification_date = data[18].split('-') # yyyy-MM-dd


class Manager:

    def __init__(self):
        print('wait...')
        with open("RU.txt", 'r') as f:
            self.data = list(
                map(
                    lambda x: Geoname(x),
                    f.read().split("\n")[:-1]
                )
            )
            self.suff_tree_root = Node()
            for geoname in self.data:
                insert(self.suff_tree_root, geoname.asciiname, str_hash(geoname.asciiname))
                for alter_name in geoname.alternatenames:
                    insert(self.suff_tree_root, alter_name, str_hash(alter_name))
        print('init completed')

    def getById(self, id):
        for geoname in self.data:
            if geoname.geonameid == id:
                return geoname
        return None

    def getGeonamesPage(self, page, limit):
        return list(
            map(
                lambda x: x.__dict__,
                self.data[min(page * limit, len(self.data) - 1) : min((page + 1) * limit, len(self.data) - 1)]
            )
        )

    def getByName(self, name):

        # при одинаковых названиях будем выбирать город с наибольшим населением
        max_population = -1
        max_geoname = None

        for geoname in self.data:
            if geoname.asciiname == name:
                if geoname.population > max_population:
                    max_geoname = geoname
                    max_population = geoname.population
            else:
                for alter_name in geoname.alternatenames:
                    if alter_name == name:
                        if geoname.population > max_population:
                            max_geoname = geoname
                            max_population = geoname.population
        
        if max_geoname is not None:
            return max_geoname
        else:
            return {}

    def getTownsWithPreffix(self, preffix):
        return keys_with_prefix(self.suff_tree_root, preffix)