from flask import Flask, request
from manager import Manager
import json

app = Flask(__name__)

manager = Manager()

#task1
@app.route("/geoname/<id>", methods=['GET'])
def get_geoname(id):
    geoname = manager.getById(int(id))
    if geoname is not None:
        return json.dumps(geoname.__dict__)
    else:
        return {}

#task2
@app.route("/geoname", methods=['GET'])
def list_geoname():
    page = int(request.args.get("page"))
    limit = int(request.args.get("limit"))
    return json.dumps(manager.getGeonamesPage(page, limit))

#task3
@app.route("/geoname/comparison", methods=['GET'])
def compare_geos():
    geo1 = manager.getByName(request.args.get("name1"))
    geo2 = manager.getByName(request.args.get("name2"))

    # какой северней?

    if geo1.latitude > geo2.latitude:
        northern = geo1.name
    else:
        northern = geo2.name

    # одинаковая ли временная зона?

    if geo1.timezone == geo2.timezone:
        is_timezone_equal = True
    else:
        is_timezone_equal = False

    return {
        'town1_info' : geo1.__dict__,
        'town2_info' : geo2.__dict__,
        'located northerly' : northern,
        'is_timezone_equal' : is_timezone_equal,
    }

#task5
@app.route("/geoname/autocomplete", methods=['GET'])
def list_towns_with_preffix():
    preffix = request.args.get("preffix")
    return json.dumps(manager.getTownsWithPreffix(preffix))
